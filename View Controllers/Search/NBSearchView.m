//
//  NBSearchView.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBSearchView.h"
/* Required */
#import "NBNumberItem.h"
#import "NBColors.h"
#import "NBFonts.h"
#import "NBTabSelector.h"
#import "NBCopy.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface NBSearchView () <NBTabSelectorDelegate,UITextFieldDelegate>
@property (nonatomic,strong) UIView *searchView;
@property (nonatomic,strong) NBTabSelector *tabSelector;
@property (nonatomic,strong) UITextField *searchTextField;
@property (nonatomic,strong) UIView *loadingView;
@property (nonatomic,strong) UILabel *resultView;
@property (nonatomic,strong) UIView *errorView;
@property (nonatomic,strong) AVAudioPlayer *audioPlayer;
@end

@implementation NBSearchView

@synthesize searchView,loadingView,resultView,errorView;
@synthesize searchTextField,tabSelector;
@synthesize searchViewDelegate;
@synthesize audioPlayer;

#pragma mark - Initialize
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

#pragma mark - Private
- (void)setViewModel:(NBSearchViewModel *)viewModel
{
    _viewModel = viewModel;
}

- (void)setUp
{
    [self setUpBackgroundColor];
    [self setUpSearchView];
    [self setUpResultView];
    [self setUpErroView];
    [self setUpLoadingView];
}

- (void)setUpBackgroundColor
{
    self.backgroundColor = [UIColor clearColor];
}

- (void)setUpSearchView
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(.0f,.0f,self.frame.size.width,190.f)];
    [self addSubview:backgroundView];
    backgroundView.backgroundColor = [UIColor flatPrimaryBlue];

    self.searchView = [[UIView alloc] initWithFrame:CGRectMake(-self.frame.size.width,50.f,self.frame.size.width,140.f)];
    [self addSubview:self.searchView];
    self.searchView.backgroundColor = [UIColor clearColor];

    self.tabSelector = [[NBTabSelector alloc] initWithFrame:CGRectMake(.0f,.0f,self.searchView.frame.size.width,50.f) tabs:@[@"Number",@"Year"]];
    [self.searchView addSubview:self.tabSelector];
    self.tabSelector.delegate = self;
    self.tabSelector.tabColor = [UIColor flatSecondaryBlue];

    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(10.f,self.tabSelector.frame.origin.y + self.tabSelector.frame.size.height + 10.f,self.searchView.frame.size.width - 80.f,40.f)];
    [self.searchView addSubview:self.searchTextField];
    self.searchTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.searchTextField.font = [UIFont setNBMainFontWithSize:15.f];
    self.searchTextField.placeholder = @"Number";
    self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.searchTextField.delegate = self;
    self.searchTextField.backgroundColor = [UIColor whiteColor];

    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchView addSubview:searchButton];
    searchButton.frame = CGRectMake(self.searchView.frame.size.width - 60.f,self.searchTextField.frame.origin.y,40.f,40.f);
    [searchButton setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *randomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchView addSubview:randomButton];
    randomButton.frame = CGRectMake(self.searchView.frame.size.width - 135.f,self.searchTextField.frame.origin.y + self.searchTextField.frame.size.height,120.f,40.f);
    [randomButton setTitle:@"I'm feeling lucky" forState:UIControlStateNormal];
    randomButton.titleLabel.font = [UIFont setNBMainFontWithSize:18.f];
    [randomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [randomButton addTarget:self action:@selector(randomButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(self.searchView.frame.origin.x,self.searchView.frame.size.height - .5f,self.searchView.frame.size.width,.5f)];
    [self.searchView addSubview:separatorView];
    separatorView.backgroundColor = [UIColor flatSecondaryBlue];

    UIView *logoBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(.0f,.0f,self.frame.size.width,50.f)];
    [self addSubview:logoBackgroundView];
    logoBackgroundView.backgroundColor = [UIColor flatSecondaryBlue];

    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width / 2 - 112.f,72.5f,224.f,40.f)];
    [self addSubview:logoView];
    logoView.image = [UIImage imageNamed:@"logo"];
    [UIView animateWithDuration:0.2 delay:1 options:UIViewAnimationOptionCurveEaseOut animations:^{
        logoView.frame = CGRectMake((self.frame.size.width / 2) - 112.f,5.f,224.f,40.f);
        logoView.transform = CGAffineTransformMakeScale(.5f,.5f);
    } completion:^(BOOL finished) {
        if (finished) {
            CGRect frame = self.searchView.frame;
            frame.origin.x = .0f;
            [UIView animateWithDuration:0.2 animations:^{
                self.searchView.frame = frame;
            }];
        }
    }];
}

- (void)setUpErroView
{
    self.errorView = [[UIView alloc] initWithFrame:CGRectMake(20.f,self.searchView.frame.origin.y + self.searchView.frame.size.height + 10.f,self.frame.size.width - 40.f,self.frame.size.height - self.searchView.frame.size.height - 100.f)];
    [self addSubview:self.errorView];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.errorView.frame.size.width / 2) - 60.f,20.f,120.f,120.f)];
    [self.errorView addSubview:imageView];
    [imageView setTintColor:[UIColor grayColor]];

    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(.0f,imageView.frame.origin.y + imageView.frame.size.height + 10.f,self.errorView.frame.size.width,100.f)];
    [self.errorView addSubview:labelView];
    labelView.font = [UIFont setNBSecondaryFontWithSize:18.f];
    labelView.text = @"";
    labelView.numberOfLines = 0;
    labelView.textAlignment = NSTextAlignmentCenter;
    labelView.textColor = [UIColor grayColor];
}

- (void)setUpLoadingView
{
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(20.f,self.searchView.frame.origin.y + self.searchView.frame.size.height + 10.f,self.frame.size.width - 40.f,self.frame.size.height - self.searchView.frame.size.height - 100.f)];
    [self addSubview:self.loadingView];
    [self.loadingView setHidden:YES];

    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(self.loadingView.frame.size.width - 30.f,.0f,40.f,40.f);
    [self.loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    [activityIndicator setTintColor:[UIColor grayColor]];
}

- (void)setUpResultView
{
    self.resultView = [[UILabel alloc] initWithFrame:CGRectMake(20.f,self.searchView.frame.origin.y + self.searchView.frame.size.height + 10.f,self.frame.size.width - 40.f,self.frame.size.height - self.searchView.frame.size.height - 100.f)];
    [self addSubview:self.resultView];
    self.resultView.font = [UIFont setNBSecondaryFontWithSize:25.f];
    self.resultView.text = @"";
    self.resultView.numberOfLines = 0;
    self.resultView.textAlignment = NSTextAlignmentCenter;
}

- (void)randomButtonTapped:(id)sender
{
    [self.searchTextField resignFirstResponder];
    if (searchViewDelegate)
        [searchViewDelegate didTapRandomFactButton];
}

- (void)searchButtonTapped:(id)sender
{
    [self.searchTextField resignFirstResponder];
    if (searchViewDelegate)
        [searchViewDelegate didTapDoSearch:self.searchTextField.text];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.resultView.text = @"";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [self searchButtonTapped:nil];
        return NO;
    }
    return YES;
}

#pragma mark - Public
- (void)updateView
{
    self.searchTextField.text = [NSString stringWithFormat:@"%d",self.viewModel.numberItem.number];
    [self.tabSelector updateCurrentTab:self.viewModel.numberItem.type];
}

- (void)showLoadingView
{
    [self.loadingView setHidden:NO];
}

- (void)showErrorView:(NSString *)error
{
    [self.loadingView setHidden:YES];
    [self.errorView.subviews enumerateObjectsUsingBlock:^(id subview,NSUInteger idx,BOOL *stop) {
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *labelView = (UILabel *)subview;
            labelView.text = error;
        } else if ([subview isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *)subview;
            if ([error isEqualToString:kNBCopy_Error_Connection]) imageView.image = [UIImage imageNamed:@"connection"];
            else imageView.image = [UIImage imageNamed:@"error"];
        }
    }];
}

- (void)showResultView
{
    [self playSound:@"searched"];
    [self.loadingView setHidden:YES];
    self.resultView.text = self.viewModel.numberItem.fact;
}

#pragma mark - Sounds
- (void)playSound:(NSString *)resource
{
    NSData *fileData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:resource ofType:@"mp3"]];
    NSError *error = nil;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithData:fileData error:&error];
    if (!error) {
        [self.audioPlayer play];
    }
}

#pragma mark - NBTabSelector Delegate
- (void)didTapTab:(int)index
{
    kNBItemType type = kNBTypeTrivia;
    switch (index) {
        case 0:
            type = kNBTypeMath;
            break;
        case 1:
            type = kNBTypeYear;
            break;
    }
    if (searchViewDelegate)
        [searchViewDelegate didTapTypeTab:type];
}

@end

@implementation NBSearchViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end