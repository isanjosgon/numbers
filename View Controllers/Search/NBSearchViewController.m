//
//  NBSearchViewController.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBSearchViewController.h"
/* Required */
#import "NBSearchView.h"
#import "NBNumberItem.h"

@interface NBSearchViewController () <NBSearchViewDelegate>
@property (nonatomic,strong) NBSearchView *searchView;
@end

@implementation NBSearchViewController

#pragma mark - Layout
- (void)loadView
{
    [super loadView];

    self.view.backgroundColor = [UIColor whiteColor];

    NBSearchViewModel *viewModel = [NBSearchViewModel new];
    viewModel.userSearch = @"";
    viewModel.numberItem = [[NBNumberItem alloc] initWithType:kNBTypeMath];

    self.searchView = [[NBSearchView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.searchView];
    self.searchView.viewModel = viewModel;
    self.searchView.searchViewDelegate = self;
}

#pragma mark - Lifecicle
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self.searchView.viewModel addObserver:self forKeyPath:@"userSearch" options:kNilOptions context:nil];
    [self.searchView.viewModel.numberItem addObserver:self forKeyPath:@"number" options:kNilOptions context:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [self removeObserver:self forKeyPath:@"userSearch"];
    [self removeObserver:self forKeyPath:@"number"];
}

#pragma mark - Private
- (void)doSearch
{
    [self.searchView showLoadingView];
    __weak typeof(self) wSelf = self;
    [self.searchView.viewModel.numberItem performSearchWithCompletionBlock:^(BOOL success,NSString *error) {
        typeof(self) sSelf = wSelf;
        if (!success)
            return [sSelf.searchView showErrorView:error];
        [sSelf.searchView showResultView];
    }];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"number"]) {
        [self.searchView updateView];
    } else if ([keyPath isEqualToString:@"userSearch"]) {
        [self.searchView showLoadingView];
        [self doSearch];
    }
}

#pragma mark - NBSearchView Delegate
- (void)didTapRandomFactButton
{
    [self.searchView showLoadingView];
    __weak typeof(self) wSelf = self;
    [self.searchView.viewModel.numberItem performRandomSearchWithCompletionBlock:^(BOOL success,NSString *error) {
        typeof(self) sSelf = wSelf;
        if (!success)
            return [sSelf.searchView showErrorView:error];
        [sSelf.searchView showResultView];
    }];
}

- (void)didTapTypeTab:(kNBItemType)type
{
    self.searchView.viewModel.numberItem.type = type;
}

- (void)didTapDoSearch:(NSString *)searchText
{
    self.searchView.viewModel.numberItem.number = [searchText intValue];
    self.searchView.viewModel.userSearch = searchText;
}

@end
