//
//  NBSearchView.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <UIKit/UIKit.h>
/* Required */
#import "NBNumberItem.h"

@class NBNumberItem;
@class NBSearchViewModel;

@protocol NBSearchViewDelegate <NSObject>
@required
- (void)didTapRandomFactButton;
- (void)didTapTypeTab:(kNBItemType)type;
- (void)didTapDoSearch:(NSString *)searchText;
@end

@interface NBSearchView : UIView
@property (nonatomic,weak) id<NBSearchViewDelegate> searchViewDelegate;
@property (nonatomic,strong) NBSearchViewModel *viewModel;
- (void)updateView;
- (void)showLoadingView;
- (void)showErrorView:(NSString *)error;
- (void)showResultView;
@end

@interface NBSearchViewModel : NSObject
@property (nonatomic,strong) NSString *userSearch;
@property (nonatomic,strong) NBNumberItem *numberItem;
@end