//
//  NBTabSelector.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  NBTabSelectorDelegate <NSObject>
@required
- (void)didTapTab:(int)index;
@end

@interface NBTabSelector : UIView
@property (nonatomic,weak) id<NBTabSelectorDelegate> delegate;
@property (nonatomic,strong) UIColor *tabColor;
- (id)initWithFrame:(CGRect)frame tabs:(NSArray *)tabs;
- (void)updateCurrentTab:(int)index;
@end
