//
//  NBTabSelector.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBTabSelector.h"
/* Required */
#import "NBColors.h"
#import "NBFonts.h"

@interface NBTabSelector ()
@property (nonatomic,strong) UIView *tabView;
@property (nonatomic,strong) NSMutableArray *tabs;
@property (nonatomic,assign) int currentTab;
@end

@implementation NBTabSelector

@synthesize tabs;
@synthesize currentTab;
@synthesize tabView;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame tabs:(NSArray *)t
{
    self = [super initWithFrame:CGRectMake(.0f,.0f,frame.size.width,frame.size.height)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        self.tabs = [[NSMutableArray alloc] initWithArray:t];

        CGFloat tabWidth = frame.size.width / [tabs count];

        self.tabView = [[UIView alloc] initWithFrame:CGRectMake(.0f,frame.size.height - 4.f,tabWidth,4.f)];
        [self addSubview:self.tabView];

        __block float x = .0f;
        [tabs enumerateObjectsUsingBlock:^(NSString *obj,NSUInteger idx,BOOL *stop) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [self addSubview:button];
            button.tag = idx;
            button.frame = CGRectMake(x,.0f,tabWidth,frame.size.height - 4.f);

            if ([tabs[idx] isKindOfClass:[NSString class]]) [button setTitle:tabs[idx] forState:UIControlStateNormal];
            else [button setTitle:[NSString stringWithFormat:@"Tab %d",(int)idx] forState:UIControlStateNormal];

            button.titleLabel.font = [UIFont setNBSecondaryFontWithSize:22.f];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(handleTap:) forControlEvents:UIControlEventTouchUpInside];

            x += tabWidth;
        }];
    }
    return self;
}

- (void)handleTap:(id)sender
{
    UIButton *button = sender;
    [self updateCurrentTab:(int)button.tag];
    if (delegate)
        [delegate didTapTab:(int)button.tag];
}

- (void)setTabColor:(UIColor *)tabColor
{
    self.tabView.backgroundColor = tabColor;
}

- (void)updateCurrentTab:(int)index
{
    self.currentTab = index;
    CGFloat tabWidth = self.frame.size.width / [self.tabs count];
    CGRect frame = self.tabView.frame;
    frame.origin.x = self.currentTab * tabWidth;
    [UIView animateWithDuration:0.2f delay:.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.tabView.frame = frame;
    } completion:nil];
}

@end
