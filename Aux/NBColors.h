//
//  NBColors.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (NBColors)
+ (UIColor *)flatPrimaryBlue;
+ (UIColor *)flatSecondaryBlue;
+ (UIColor *)flatPrimaryGreen;
+ (UIColor *)flatSecondaryGreen;
@end
