//
//  NBCopy.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#define kRESTfulResource_Type_Math @"math"
#define kRESTfulResource_Type_Year @"year"
#define kRESTfulResource_Type_Trivia @"trivia"
#define kRESTfulResource_Type_Date @"date"

#define kRESTfulResource_Number_Random @"random"

#define kNBCopy_Error_Connection @"Oops! No network connection :("
#define kNBCopy_Error_Request @"Oops! Something had happened :S"