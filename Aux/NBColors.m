//
//  NBColors.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBColors.h"

@implementation UIColor (NBColors)

+ (UIColor *)flatPrimaryBlue
{
    return [self colorWithRed:42.f/255.f green:153.f/255.f blue:214.f/255.f alpha:1.f];
}

+ (UIColor *)flatSecondaryBlue
{
    return [self colorWithRed:32.f/255.f green:129.f/255.f blue:181.f/255.f alpha:1.f];
}

+ (UIColor *)flatPrimaryGreen
{
    return [self colorWithRed:.0f/255.f green:188.f/255.f blue:158.f/255.f alpha:1.f];
}

+ (UIColor *)flatSecondaryGreen
{
    return [self colorWithRed:.0f/255.f green:160.f/255.f blue:135.f/255.f alpha:1.f];
}

@end
