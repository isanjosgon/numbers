//
//  NBFonts.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBFonts.h"

@implementation UIFont (NBFonts)

+ (UIFont *)setNBMainFontWithSize:(float)size
{
    return [self fontWithName:@"OmnesRegular-Roman" size:size];
}

+ (UIFont *)setNBSecondaryFontWithSize:(float)size
{
    return [self fontWithName:@"OmnesSemibold-Roman" size:size];
}

@end
