//
//  NBReachabilityProvider.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBReachabilityProvider.h"
/* Required */
#import "Reachability.h"

static Reachability *reachability;

@implementation NBReachabilityProvider

+ (void)startMonitoring
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
}

+ (BOOL)isReachable
{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    return (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN);
}

+ (void)reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN) {
        NSLog(@"Connection again :)"); // TODO: Update UI & Feedback to the user
    }
}

@end
