//
//  NBAPIProvider.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBAPIProvider.h"
/* Required */
#import "AFNetworking.h"
#import "NBCopy.h"

#pragma mark - Constants
static NSString *const kNBRESTful = @"http://numbersapi.com/";

@implementation NBAPIProvider

#pragma mark - Public
+ (void)requestGETFactTypeTriviaWithNumber:(int)number completionBlock:(void(^)(id results,BOOL success))completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[self getURL:kNBTypeTrivia params:[NSNumber numberWithInt:number]] parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        if (completionBlock) completionBlock(responseString,YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) completionBlock(error,NO);
    }];
}

+ (void)requestGETFactTypeMathWithNumber:(int)number completionBlock:(void(^)(id results,BOOL success))completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[self getURL:kNBTypeMath params:[NSNumber numberWithInt:number]] parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        if (completionBlock) completionBlock(responseString,YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) completionBlock(error,NO);
    }];
}

+ (void)requestGETFactTypeDateWithMonth:(int)month day:(int)day completionBlock:(void(^)(id results,BOOL success))completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[self getURL:kNBTypeDate params:@{@"month":[NSNumber numberWithInt:month],@"day":[NSNumber numberWithInt:day]}] parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        if (completionBlock) completionBlock(responseString,YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) completionBlock(error,NO);
    }];
}

+ (void)requestGETFactTypeYearWithYear:(int)year completionBlock:(void(^)(id results,BOOL success))completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[self getURL:kNBTypeYear params:[NSNumber numberWithInt:year]] parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        if (completionBlock) completionBlock(responseString,YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) completionBlock(error,NO);
    }];
}

+ (void)requestGETRandomFactWithCompletionBlock:(void(^)(id fact,NSString *number,NSString *type,BOOL success))completionBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[self getURL:kNBTypeRandom params:nil] parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *headers = [[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
        NSString *randomNumber = [headers objectForKey:@"X-Numbers-API-Number"];
        NSString *randomType = [headers objectForKey:@"X-Numbers-API-Type"];
        if (completionBlock) completionBlock(responseString,randomNumber,randomType,YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) completionBlock(error,nil,nil,NO);
    }];
}

#pragma mark - Private
+ (NSString *)getURL:(kNBItemType)endpoint params:(id)params
{
    NSString *endpointURL;
    switch (endpoint) {
        case kNBTypeTrivia:
            endpointURL = [NSString stringWithFormat:@"%d",[params intValue]];
            break;
        case kNBTypeMath:
            endpointURL = [NSString stringWithFormat:@"%d/%@",[params intValue],kRESTfulResource_Type_Math];
            break;
        case kNBTypeYear:
            endpointURL = [NSString stringWithFormat:@"%d/%@",[params intValue],kRESTfulResource_Type_Year];
            break;
        case kNBTypeDate:
            endpointURL = [NSString stringWithFormat:@"%d/%d/%@",[[params objectForKey:@"month"] intValue],[[params objectForKey:@"day"] intValue],kRESTfulResource_Type_Date];
            break;
        default:
            endpointURL = [NSString stringWithFormat:@"%@/%@",kRESTfulResource_Number_Random,[self convertToStringFromType:arc4random() % 2]];
            break;
    }
    return [NSString stringWithFormat:@"%@%@",kNBRESTful,endpointURL];
}

+ (NSString *)convertToStringFromType:(kNBItemType)t
{
    NSString *typeString;
    switch (t) {
        case kNBTypeMath:
            typeString = kRESTfulResource_Type_Math;
            break;
        case kNBTypeTrivia:
            typeString = kRESTfulResource_Type_Trivia;
            break;
        case kNBTypeYear:
            typeString = kRESTfulResource_Type_Year;
            break;
        case kNBTypeDate:
            typeString = kRESTfulResource_Type_Date;
            break;
        default:
            break;
    }
    return typeString;
}

+ (kNBItemType)convertToTypeFromString:(NSString *)t
{
    kNBItemType type;
    if ([t isEqualToString:kRESTfulResource_Type_Math])
        type = kNBTypeMath;
    else if ([t isEqualToString:kRESTfulResource_Type_Trivia])
        type = kNBTypeTrivia;
    else if ([t isEqualToString:kRESTfulResource_Type_Year])
        type = kNBTypeYear;
    else if ([t isEqualToString:kRESTfulResource_Type_Date])
        type = kNBTypeDate;
    return type;
}

@end
