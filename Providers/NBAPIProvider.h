//
//  NBAPIProvider.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <Foundation/Foundation.h>
/* Required */
#import "NBNumberItem.h"

@interface NBAPIProvider : NSObject
#pragma mark - Request
+ (void)requestGETFactTypeTriviaWithNumber:(int)number completionBlock:(void(^)(id results,BOOL success))completionBlock;
+ (void)requestGETFactTypeMathWithNumber:(int)number completionBlock:(void(^)(id results,BOOL success))completionBlock;
+ (void)requestGETFactTypeDateWithMonth:(int)month day:(int)day completionBlock:(void(^)(id results,BOOL success))completionBlock;
+ (void)requestGETFactTypeYearWithYear:(int)year completionBlock:(void(^)(id results,BOOL success))completionBlock;
+ (void)requestGETRandomFactWithCompletionBlock:(void(^)(id fact,NSString *number,NSString *type,BOOL success))completionBlock;
#pragma mark - Helpers
+ (NSString *)convertToStringFromType:(kNBItemType)type;
+ (kNBItemType)convertToTypeFromString:(NSString *)type;
@end
