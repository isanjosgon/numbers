//
//  NBReachabilityProvider.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NBReachabilityProvider : NSObject
+ (void)startMonitoring;
+ (BOOL)isReachable;
@end
