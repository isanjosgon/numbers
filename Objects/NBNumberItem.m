//
//  NBNumberItem.m
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import "NBNumberItem.h"
/* Required */
#import "NBAPIProvider.h"
#import "NBReachabilityProvider.h"
#import "NBCopy.h"

@implementation NBNumberItem

@synthesize type,fact,number;

#pragma mark - Public
- (id)initWithType:(kNBItemType)t
{
    self = [super init];
    if (self) {
        self.type = t;
    }
    return self;
}

- (void)performSearchWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock
{
    if (![NBReachabilityProvider isReachable]) {
        return completionBlock(NO,kNBCopy_Error_Connection);
    }
    [self searchByTypeWithCompletionBlock:completionBlock];
}

- (void)performRandomSearchWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock
{
    if (![NBReachabilityProvider isReachable]) {
        return completionBlock(NO,kNBCopy_Error_Connection);
    }

    [NBAPIProvider requestGETRandomFactWithCompletionBlock:^(id result,NSString *n,NSString *t,BOOL success) {
        self.type = [NBAPIProvider convertToTypeFromString:t];
        self.number = [n intValue];
        if (!success) return [self onRequestFailureWithCompletionBlock:completionBlock];
        [self onRequestSuccessWithData:result completionBlock:completionBlock];
    }];
}

#pragma mark - Private
- (void)searchByTypeWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock
{

    if (self.type == kNBTypeTrivia) {
        [NBAPIProvider requestGETFactTypeTriviaWithNumber:self.number completionBlock:^(id results,BOOL success) {
            if (!success) return [self onRequestFailureWithCompletionBlock:completionBlock];
            [self onRequestSuccessWithData:results completionBlock:completionBlock];
        }];
    } else if (self.type == kNBTypeMath) {
        [NBAPIProvider requestGETFactTypeMathWithNumber:self.number completionBlock:^(id results,BOOL success) {
            if (!success) return [self onRequestFailureWithCompletionBlock:completionBlock];
            [self onRequestSuccessWithData:results completionBlock:completionBlock];
        }];
    } else if (self.type == kNBTypeYear) {
        [NBAPIProvider requestGETFactTypeYearWithYear:self.number completionBlock:^(id results,BOOL success) {
            if (!success) return [self onRequestFailureWithCompletionBlock:completionBlock];
            [self onRequestSuccessWithData:results completionBlock:completionBlock];
        }];
    }
}

- (void)onRequestSuccessWithData:(id)resutls completionBlock:(void(^)(BOOL success,NSString *error))completionBlock
{
    if ([resutls isKindOfClass:[NSString class]]) self.fact = resutls;
    completionBlock(YES,nil);
}

- (void)onRequestFailureWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock
{
    completionBlock(NO,kNBCopy_Error_Request);
}

@end
