//
//  NBNumberItem.h
//  Numbers
//
//  Created by Israel San Jose Gonzalez on 05/10/2014.
//  Copyright (c) 2014 Isra San Jose. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,kNBItemType) {
    kNBTypeMath,
    kNBTypeYear,
    kNBTypeTrivia,
    kNBTypeDate,
    kNBTypeRandom
};

@interface NBNumberItem : NSObject
@property (nonatomic,assign) kNBItemType type;
@property (nonatomic,strong) NSString *fact;
@property (nonatomic,assign) int number;
- (id)initWithType:(kNBItemType)type;
- (void)performSearchWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock;
- (void)performRandomSearchWithCompletionBlock:(void(^)(BOOL success,NSString *error))completionBlock;
@end
